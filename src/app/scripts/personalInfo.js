var personalInfo = {
    name: "Gabriel Miranda",
    mail: "gabrielmlima@gmail.com",
    phone: "600 041 604",
};

var onLoadPersonalInfo = () => {
    console.log('onLoadPersonalInfo');
    var myName = document.getElementById("personal_info_name");
    myName.innerHTML = personalInfo.name;

    var myMail = document.getElementById("personal_info_mail");
    myMail.innerHTML = personalInfo.mail;

    var myPhone = document.getElementById("personal_info_phone");
    myPhone.innerHTML = personalInfo.phone;
}

export { onLoadPersonalInfo };