var studies = [{
        studieName: "Licenciatura en Administración de Empresas",
        studyCenter: "Facultad Ruy Barbosa (Brasil)",
        date: "2003",
    },
    {
        studieName: "Post Grado en Gestión de Empresas",
        studyCenter: "IDEC - Pompeu Fabra (España)",
        date: "2005",
    },
    {
        studieName: "Bootcamp FullStack",
        studyCenter: "Upgrade Hub",
        date: "2020 - Actualmente",
    }
];

var onLoadStudies = () => {
    console.log('onLoadStudies');
    var studiesList = document.getElementById("studies_list");
    studies.forEach((studie) => {
        // console.log(studie);
        var li = document.createElement('li');
        li.setAttribute('class', 'studies-list__item');
        li.innerHTML = studie.studieName + ' | ' + studie.stuyCenter + ' | ' + studie.date;
        studiesList.appendChild(li);
    });
}

var toggleStudies = () => {
    console.log('toggleStudies');
    var studies = document.getElementById("studies");
    if (studies.style.display === "none") {
        studies.style.display = "flex";
        experience.style.display = "none";
        language.style.display = "none";
    } else {
        studies.style.display = "none";
    }
}

export { onLoadStudies, toggleStudies };