var languages = [{
        idiom: "Español",
        skills: {
            wrtiting: "muy alto",
            speakig: "muy alto",
            reading: "muy alto",
        },
    },
    {
        idiom: "Portugues",
        skills: {
            wrtiting: "nativo",
            speakig: "nativo",
            reading: "nativo",
        },
    },
    {
        idiom: "Ingles",
        skills: {
            wrtiting: "bajo",
            speakig: "intermedio",
            reading: "muy alto",
        },
    },
    {
        idiom: "Catalan",
        skills: {
            wrtiting: "bajo",
            speakig: "bajo",
            reading: "muy alto",
        },
    }
];

var lenguageSkills = [{
        level: "nativo",
        rate: 100,
    },
    {
        level: "muy alto",
        rate: 80,
    },
    {
        level: "intermedio",
        rate: 50,
    },
    {
        level: "bajo",
        rate: 30,
    },
    {
        level: "ninguno",
        rate: 0,
    },

]

var onLoadLanguages = () => {
    var languagesList = document.getElementById("language_list");


    languages.forEach(language => {
        // console.log(language);
        var skillLanguage = language.skills;
        var values = Object.values(skillLanguage);
        var rateSum = 0;
        values.forEach(elementLevel => {


            var match = lenguageSkills.find(element => element.level === elementLevel);
            rateSum += match.rate;
        });

        var pgrLine = Math.floor(rateSum / 300 * 100);


        var divItem = document.createElement("div");
        divItem.setAttribute('class', 'language-list_item');
        divItem.innerHTML = language.idiom;

        var divProgress = document.createElement("div");
        divProgress.className = "progress";
        divProgress.innerHTML = "<div class='progress-bar' role='progressbar' style='width: " + pgrLine + "%' aria-valuemin='0' aria-valuemax='300'></div>";

        divItem.appendChild(divProgress);
        languagesList.appendChild(divItem);


    });
}

var toggleLanguage = () => {
    var languages = document.getElementById("language");
    if (language.style.display === "none") {
        language.style.display = "flex";
        experience.style.display = "none";
        studies.style.display = "none";
    } else {
        language.style.display = "none";
    }
}

export { onLoadLanguages, toggleLanguage };