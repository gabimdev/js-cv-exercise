var experience = [{
        nameJob: "Administrativo Dpto. Processo Pedidos",
        companyName: "Bandalux",
        date: "2019 - Actualmente",
    },
    {
        nameJob: "Moderador de Contenido",
        companyName: "Avarto",
        date: "2018 - 2018",
    },
    {
        nameJob: "Day Trader",
        companyName: "Trabajador por cuenta propia",
        date: "2011 - 2016",
    },
    {
        nameJob: "Fundador/Gerente",
        companyName: "Temaki-ya",
        date: "2011 - 2016",
    },
    {
        nameJob: "Asesor Financiero",
        companyName: "Catalunya Caixa",
        date: "2007 - 2011",
    },
    {
        nameJob: "Dj / Productor",
        companyName: "Part-time",
        date: "2000 - Actualmente",
    }
];

var onLoadExperience = () => {
    console.log('onLoadExperience');
    var experienceList = document.getElementById("experience_list");
    experience.forEach((job) => {
        // console.log(job);
        var li = document.createElement('li');
        li.setAttribute('class', 'experience-list__item');
        li.innerHTML = job.nameJob + ' | ' + job.companyName + ' | ' + job.date;
        experienceList.appendChild(li);
    });
}

var toggleExperience = () => {
    console.log('toggleExperience');
    var experience = document.getElementById("experience");
    if (experience.style.display === "none") {
        experience.style.display = "flex";
        language.style.display = "none";
        studies.style.display = "none";
    } else {
        experience.style.display = "none";
    }
}

export { onLoadExperience, toggleExperience };