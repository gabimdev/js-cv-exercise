var mouseoverImg = () => {
    var imgDisplayCartoon = document.getElementById("photo_cartoon");
    var imgDisplayPhoto = document.getElementById("photo");
    if (imgDisplayPhoto.style.display === "none") {
        imgDisplayPhoto.style.display = "block";
        imgDisplayCartoon.style.display = "none";
    } else {
        imgDisplayPhoto.style.display = "none";
        imgDisplayCartoon.style.display = "block";
    }

}

export { mouseoverImg };