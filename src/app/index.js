import { onLoadPersonalInfo } from './scripts/personalInfo';
import { onLoadExperience, toggleExperience } from './scripts/experience';
import { onLoadStudies, toggleStudies } from './scripts/studies';
import { onLoadLanguages, toggleLanguage } from "./scripts/languages";
import { mouseoverImg } from "./scripts/photoDsp";

import './styles/styles.scss';

var addListeners = () => {
    document.getElementById("btn_hide_experience").addEventListener("click", toggleExperience);
    document.getElementById("btn_hide_studies").addEventListener("click", toggleStudies);
    document.getElementById("btn_hide_language").addEventListener("click", toggleLanguage);
    document.getElementById("photo_cartoon").addEventListener("mouseover", mouseoverImg);
    document.getElementById("photo").addEventListener("mouseout", mouseoverImg);
}

window.onload = () => {
    onLoadPersonalInfo();
    onLoadExperience();
    onLoadStudies();
    onLoadLanguages();
    mouseoverImg();

    addListeners();
};